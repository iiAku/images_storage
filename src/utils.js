const minio = require('minio')
const redis = require('redis')

const config = require('./config')
const minioClient = new minio.Client(config.minio)
const redisClient = redis.createClient(config.redis)

//Bucket handlers
const bucketExists = (data) => {
  return new Promise((resolve, reject) => {
    const { bucketName, region } = data
    minioClient.bucketExists(bucketName, (err) => {
      if (err) {
        if (err.code == 'NoSuchBucket') {
          resolve()
        } else {
          reject(err.code)
        }
      } else {
        reject()
      }
    })
  })
}

const makeBucket = (data) => {
  return new Promise((resolve, reject) => {
    const { bucketName, region } = data
    minioClient.makeBucket(bucketName, region, (err) => {
      if (err) {
        reject({
          message: 'Error while creating bucket',
          err: err
        })
      } else {
        resolve()
      }
    })
  })
}

const bucketsHandlers = {
  exist: bucketExists,
  make: makeBucket
}

const makeDefaultBucket = (data) => {
  return new Promise((resolve, reject) => {
    bucketExists(data)
      .then(() => {
        makeBucket(data)
          .then(resolve)
          .catch(reject)
      })
      .catch(resolve)
  })
}

//Object handlers
const putObject = (file) => {
  return new Promise((resolve, reject) => {
    minioClient.putObject(file.bucketName, file.name, file.metadatas.buffer, (error, etag) => {
      if (error) {
        reject(error)
      } else {
        file.metadatas.etag = etag
        resolve({
          key: etag,
          value: file.redisify()
        })
      }
    })
  })
}

const removeObject = (data) => {
  return new Promise((resolve, reject) => {
    const { bucketName, fileName } = data
    minioClient.removeObject(bucketName, fileName, (err) => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}

const listObject = (data) => {
  return new Promise((resolve, reject) => {
    const { bucketName, prefix } = data
    const objects = []
    const stream = minioClient.listObjects(bucketName, prefix, true)
    stream.on('data', (obj) => {
      objects.push(obj)
    })
    stream.on('end', () => {
      resolve(objects)
    })
    stream.on('error', (err) => {
      reject(err)
    })
  })
}

const statObject = (data) => {
  return new Promise((resolve, reject) => {
    const { bucketName, fileName } = data
    minioClient.statObject(bucketName, fileName, (err, stat) => {
      if (err) {
        reject(err)
      } else {
        resolve(stat)
      }
    })
  })
}

const signObject = (data) => {
  return new Promise((resolve, reject) => {
    const { bucketName, objectName, expiry } = data
    minioClient.presignedUrl('GET', bucketName, objectName, expiry, (err, presignedUrl) => {
      if (err) {
        reject(err)
      } else {
        resolve(presignedUrl)
      }
    })
  })
}

const objectsHandlers = {
  put: putObject,
  remove: removeObject,
  list: listObject,
  sign: signObject,
  stat: statObject
}

//Redis handlers
const setValue = (data) => {
  return new Promise((resolve, reject) => {
    const { key, value } = data
    redisClient.hmset(key, value, (err, res) => {
      if (err) {
        reject(err)
      } else {
        resolve(value)
      }
    })
  })
}

const getValue = (key) => {
  return new Promise((resolve, reject) => {
    redisClient.hgetall(key, (err, value) => {
      if (err || value === null) {
        reject(err)
      } else {
        resolve(value)
      }
    })
  })
}

const getFileByEtag = (etag) => {
  return new Promise((resolve, reject) => {
    getValue(etag)
      .then(resolve)
      .catch(reject)
  })
}

const redisHandlers = {
  set: setValue,
  get: getValue,
  getFileByEtag: getFileByEtag
}

module.exports = {
  makeDefaultBucket,
  bucketsHandlers,
  objectsHandlers,
  redisHandlers
}
