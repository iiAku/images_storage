const _ = require('lodash')
const utils = require('./utils')
const config = require('./config')

const File = (data) => {
  const { name, path, bucketName, metadatas } = data
  const self = {
    name: name,
    path: path,
    bucketName: bucketName,
    metadatas: (metadatas) ? metadatas : null,
    signedUrl: '',
    redisify: () => {
      return {
        name: self.name,
        path: self.path,
        bucketName: self.bucketName,
        metadatas: JSON.stringify(_.omit(self.metadatas, 'buffer'))
      }
    },
    toJSON: () => {
      return {
        name: self.name,
        path: self.path,
        bucketName: self.bucketName,
        signedUrl: self.signedUrl,
        metadatas: _.omit(self.metadatas, 'buffer')
      }
    },
    setSignedUrl: () => {
      return new Promise((resolve, reject) => {
        utils.objectsHandlers.sign({
            bucketName: self.bucketName,
            objectName: self.name,
            expiry: config.expiry
          }).then(signedUrl => {
            self.signedUrl = signedUrl
            resolve(self.toJSON())
          })
          .catch(err => reject('We failed generating a signed url, for this file ressource ( ' + err + ' )'))
      })
    }
  }
  self.metadatas.created = Date.now()
  return self
}

module.exports = File
