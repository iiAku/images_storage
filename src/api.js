const express = require('express')
const router = express.Router()
const minio = require('minio')
const multer = require('multer')
const redis = require('redis')
const async = require('async')
const _ = require('lodash')

const config = require('./config')
const utils = require('./utils')
const File = require('./file')

const minioClient = new minio.Client(config.minio)
const redisClient = redis.createClient(config.redis)

router.post("/images", multer({ storage: multer.memoryStorage() }).single("upload"), (req, res) => {
  const metadatas = _.assign(req.body, _.omit(req.file, ['originalname']))
  if (req.hasOwnProperty('file')) {
    const uploadedImage = File({
      name: req.file.originalname,
      path: '',
      bucketName: config.buckets.images,
      metadatas: metadatas
    })
    //dummy extension check, it can be bypassed easily
    const ImageExtension = _.last(uploadedImage.name.split('.'))
    if (!_.isEmpty(ImageExtension) && ImageExtension.match(/^(jpg|jpeg|png|gif)$/i)) {
      utils.objectsHandlers.put(uploadedImage)
        .then(utils.redisHandlers.set)
        .then(file => res.json({ success: true, errorMessage: null, result: uploadedImage.toJSON() }))
        .catch(err => res.json({ success: false, errorMessage: 'An error happened while uploading image (' + err + ')', result: null }))
    } else {
      res.json({ success: false, errorMessage: 'Does not look like an image...', result: null })
    }
  } else {
    res.json({ success: false, errorMessage: 'There is no file data in request', result: null })
  }
})

router.get('/images/:etag', (req, res) => {
  utils.redisHandlers.get(req.params.etag)
    .then(redisValue => {
      const retrievedImage = File({
        name: redisValue.name,
        path: redisValue.path,
        bucketName: redisValue.bucketName,
        metadatas: JSON.parse(redisValue.metadatas)
      })
      utils.objectsHandlers.stat({ bucketName: retrievedImage.bucketName, fileName: retrievedImage.name })
        .then(statImage => res.json({ success: true, errorMessage: null, result: retrievedImage.toJSON() }))
        .catch(err => res.json({ success: false, errorMessage: 'An error happened while getting image data (' + err + ')', result: null }))
    })
    .catch(err => res.json({ success: false, errorMessage: 'An error happened while retrieving image (' + err + ')', result: null }))
})

router.get('/images', (req, res) => {
  utils.objectsHandlers.list({
      bucketName: config.buckets.images,
      prefix: ''
    })
    .then(images => {
      async.map(images, (image, cb) => {
        utils.redisHandlers.getFileByEtag(image.etag)
          .then(redisValue => {
            const image = File({
              name: redisValue.name,
              path: redisValue.path,
              bucketName: redisValue.bucketName,
              metadatas: JSON.parse(redisValue.metadatas)
            })
            image.setSignedUrl()
              .then(imageWithSignedUrl => cb(null, imageWithSignedUrl))
          })
      }, (err, listOfFiles) => {
        if (err) {
          reject(err)
        } else {
          res.json({ success: true, errorMessage: null, result: listOfFiles })
        }
      })
    })
    .catch(err => res.json({ success: false, errorMessage: 'An error happened while listing all images (' + err + ')', result: null }))
})

router.delete('/images/:etag', (req, res) => {
  utils.redisHandlers.get(req.params.etag)
    .then(redisValue => {
      const retrievedImage = File({
        name: redisValue.name,
        path: redisValue.path,
        bucketName: redisValue.bucketName,
        metadatas: JSON.parse(redisValue.metadatas)
      })
      utils.objectsHandlers.stat({ bucketName: retrievedImage.bucketName, fileName: retrievedImage.name })
        .then(statImage => {
          utils.objectsHandlers.remove({ bucketName: retrievedImage.bucketName, fileName: retrievedImage.name })
            .then(() => res.json({ success: true, errorMessage: null, result: retrievedImage }))
            .catch(err => res.json({ success: false, errorMessage: 'An error happened while deleting image', result: null }))
        })
        .catch(err => res.json({ success: false, errorMessage: 'Object does not exist or already deleted (' + err + ')', result: null }))
    })
    .catch(err => res.json({ success: false, errorMessage: 'An error happened while retrieving image for deleting (' + err + ')', esult: null }))
})

module.exports = router
