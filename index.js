const express = require('express')
const app = express()
const port = process.env.PORT || 1337
const cors = require('cors')
const bodyParser = require('body-parser')

const config = require('./src/config')
const api = require('./src/api.js')
const utils = require('./src/utils')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use('/', api)

utils.makeDefaultBucket({
    bucketName: config.buckets.images,
    region: config.regions.default
  })
  .then(() => app.listen(port, () => console.log('REST API listening on localhost @ ', port)))
  .catch((err) => console.log('An error happened while setting up minio s bucket', err))
