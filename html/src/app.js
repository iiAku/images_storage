var apiEndpoint = 'http://localhost:1337'

var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    name: '',
    description: '',
    file: '',
    images: []
  },
  methods: {
    uploadImage: function() {
      
    },
    removeImage: function(etag) {
      this.$http.delete(apiEndpoint + '/images/' + etag, {
        before(request) {
          if (this.previousRequest) {
            this.previousRequest.abort()
          }
          this.previousRequest = request
        }
      }).then(response => {
        if (response.body.success) {
          this.images = this.images.filter(image => image.metadatas.etag !== etag)
          console.log('image successfuly removed')
        } else {
          console.log('an error happened')
        }
      }, response => {
        console.log('an error happened')
      })
    }
  },
  beforeMount: function() {
    this.$http.get(apiEndpoint + '/images', {
      before(request) {
        if (this.previousRequest) {
          this.previousRequest.abort()
        }
        this.previousRequest = request
      }
    }).then(response => {
      if (response.body.success) {
        this.images = response.body.result
      } else {
        console.log('an error happened')
      }
    }, response => {
      console.log('an error happened')
    })
  }
})
